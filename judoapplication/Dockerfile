FROM gradle:4.7.0-jdk8-alpine as builder

USER root
RUN mkdir -p /app
WORKDIR /app

RUN chmod -R 0776 /app
RUN chown -R gradle /app
USER gradle

RUN mkdir -p /app/.cache
ENV GRADLE_USER_HOME=/app/.cache

COPY *.gradle /app/
COPY gradle/ /app/gradle
RUN gradle

COPY /src/. /app/src
RUN gradle build --stacktrace

FROM openjdk:8-jre-alpine

ARG USER=user
RUN addgroup -S -g 1001 ${USER}
RUN mkdir /app
RUN adduser -D -S -H -G ${USER} -u 1001 -s /bin/false -h /app ${USER}
RUN chown -R ${USER}:${USER} /app

COPY --from=builder /app/build/libs/app.jar /app/app.jar
COPY entrypoint.sh /bin/entrypoint.sh
ARG OPTS
ENV JAVA_OPTS=$OPTS
ENTRYPOINT ./bin/sh /bin/entrypoint.sh $JAVA_OPTS