package hu.szegedijudose.judoapplication.competition.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Location {

    private Long id;
    private String city;
    private String address;
    private String establishment;
}
