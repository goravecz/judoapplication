package hu.szegedijudose.judoapplication.competition.service;

import hu.szegedijudose.judoapplication.competition.dao.CompetitionDao;
import hu.szegedijudose.judoapplication.competition.domain.Competition;
import hu.szegedijudose.judoapplication.competition.domain.CompetitionQuery;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CompetitionService {

    private CompetitionDao competitionDao;

    public CompetitionService(CompetitionDao competitionDao) {
        this.competitionDao = competitionDao;
    }

    public Long addCompetition(Competition competition) {
        return competitionDao.addCompetition(competition);
    }

    public Competition getCompetition(Long id) {
        return competitionDao.getCompetition(id);
    }

    public List<Competition> searchCompetitions(CompetitionQuery query) {
        return competitionDao.searchCompetitions(query);
    }

    public Long updateCompetition(Competition competition) {
        return competitionDao.updateCompetition(competition);
    }

    public void deleteCompetition(Long id) {
        competitionDao.deleteCompetition(id);
    }
}
