package hu.szegedijudose.judoapplication.competition.domain;

import lombok.Value;

import java.time.LocalDate;

@Value
public class CompetitionQuery {

    private Long id;
    private String name;
    private LocalDate fromDate;
    private LocalDate toDate;
    private String city;
    private String athlete;
    private Integer rank;
}
