package hu.szegedijudose.judoapplication.competition.dao;

import hu.szegedijudose.judoapplication.competition.domain.Competition;
import hu.szegedijudose.judoapplication.competition.domain.CompetitionQuery;
import hu.szegedijudose.judoapplication.competition.domain.Location;
import hu.szegedijudose.judoapplication.competition.domain.Result;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CompetitionDao {

    public Long addCompetition(Competition competition) {
        return getDummyId();
    }

    public Competition getCompetition(Long id) {
        List<Result> dummyResults = getDummyResults();
        return getFirstDummyCompetition(dummyResults);
    }

    public List<Competition> searchCompetitions(CompetitionQuery query) {
        return createDummySearchResponse();
    }

    public Long updateCompetition(Competition competition) {
        return getDummyId();
    }

    public void deleteCompetition(Long id) {
        System.out.println("Resource with id: " + id + " is deleted.");
    }

    private List<Competition> createDummySearchResponse() {
        List<Competition> dummyResponse = new ArrayList<>();
        List<Result> dummyResults = getDummyResults();
        Competition firstDummyCompetition = getFirstDummyCompetition(dummyResults);
        Competition secondDummyCompetition = getSecondDummyCompetition(dummyResults);
        dummyResponse.add(firstDummyCompetition);
        dummyResponse.add(secondDummyCompetition);

        return dummyResponse;
    }

    private Competition getFirstDummyCompetition(List<Result> dummyResults) {
        return new Competition(
                    1L,
                    "firstCompetition",
                    LocalDateTime.of(2016, 4, 22, 10, 0),
                    new Location(1L, "Szeged", "Topolya sor 12.", "SZTE Sportközpont Judo terem"),
                    dummyResults,
                    "Nevezési díj: 1 000.- Ft"
            );
    }

    private Competition getSecondDummyCompetition(List<Result> dummyResults) {
        return new Competition(
                2L,
                "secondCompetition",
                LocalDateTime.of(2016, 5, 15, 9, 0),
                new Location(1L, "Nagykőrös", "József Attila utca 20.", "József Attila Általános Iskola"),
                dummyResults,
                "Nevezési díj: 1 500.- Ft"
        );
    }

    private List<Result> getDummyResults() {
        List<Result> dummyResults = new ArrayList<>();
        dummyResults.add(new Result(1L, "XY", 5, 0L));
        dummyResults.add(new Result(2L, "QW", 3, 0L));

        return dummyResults;
    }

    private Long getDummyId() {
        return 1L;
    }
}