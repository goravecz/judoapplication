package hu.szegedijudose.judoapplication.competition.controller;

import hu.szegedijudose.judoapplication.competition.domain.Competition;
import hu.szegedijudose.judoapplication.competition.domain.CompetitionQuery;
import hu.szegedijudose.judoapplication.competition.service.CompetitionService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(value = "/competitions")
public class CompetitionController {

    private CompetitionService competitionService;

    public CompetitionController(CompetitionService competitionService) {
        this.competitionService = competitionService;
    }

    @PostMapping(value = "/", consumes = "application/json", produces="application/json")
    public ResponseEntity<Map<String, Long>> addCompetition(@RequestBody Competition competition) {
        Long id = competitionService.addCompetition(competition);
        Map<String, Long> response = new HashMap<>();
        response.put("id", id);

        return ResponseEntity.ok().body(response);
    }

    @GetMapping(value = {"/", "/{id}"}, produces = "application/json")
    public ResponseEntity<Competition> getCompetition(@PathVariable(name = "id", required = false) Long id) {
        Competition competition = competitionService.getCompetition(id);

        return ResponseEntity.ok().body(competition);
    }

    @GetMapping(value = "/search", consumes = "application/json", produces="application/json")
    public ResponseEntity<List<Competition>> searchCompetitions(CompetitionQuery query) {
        List<Competition> searchResults = competitionService.searchCompetitions(query);

        return ResponseEntity.ok().body(searchResults);
    }

    @PatchMapping(value = "/", consumes = "application/json", produces="application/json")
    public ResponseEntity<Map<String, Long>> updateCompetition(@RequestBody Competition competition) {
        Long id = competitionService.updateCompetition(competition);
        Map<String, Long> response = new HashMap<>();
        response.put("id", id);

        return ResponseEntity.ok().body(response);
    }

    @DeleteMapping(value = "/{id}", produces="application/json")
    public ResponseEntity<Long> deleteCompetition(@PathVariable("id") Long id) {
        competitionService.deleteCompetition(id);

        return ResponseEntity.ok().build();
    }
}
