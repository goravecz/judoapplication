package hu.szegedijudose.judoapplication.competition.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Competition {

    private Long id;
    private String name;
    private LocalDateTime dateAndTime;
    private Location location;
    private List<Result> results;
    private String additionalInformation;
}
