#!/bin/sh
echo "Environment: $1"
if [ "$1" = "prod" ]; then
    java -Djava.security.egd=file:/dev/./urandom -jar app/app.jar
else
	java -agentlib:jdwp=transport=dt_socket,address=5005,suspend=n,server=y -Djava.security.egd=file:/dev/./urandom -jar app/app.jar
fi
