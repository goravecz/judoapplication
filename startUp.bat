@echo off

if "%~1"=="-FIXED_CTRL_C" (
   SHIFT
) ELSE (
   CALL <NUL %0 -FIXED_CTRL_C %*
   GOTO :EOF
)

if "%1" == "dev" (
	docker-compose -f docker-compose.dev.yml build
	docker-compose -f docker-compose.dev.yml up
) 
if "%1" == "prod" (
	docker-compose -f docker-compose.prod.yml build --build-arg OPTS=prod
	docker-compose -f docker-compose.dev.yml up
)